// Aggregate to count the total number of items supplied by Red Farms
db.fruits.aggregate([
  { $match: { supplier: "Red Farms Inc" } },
  { $count: "totalNumberOfFruits" },
]);

// Aggregate to count the total number of items supplied by Green Farming
db.fruits.aggregate([
  { $match: { supplier: "Green Farming and Canning" } },
  { $count: "totalNumberOfFruits" },
]);

// Aggregate to get the average price of all fruits that are onSale per supplier.
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier", avgPrice: { $avg: "$price" } } },
]);

// Aggregate to get the highest price of a fruit per supplier.
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier", highestPrice: { $max: "$price" } } },
]);

// Aggregate to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier", lowestPrice: { $min: "$price" } } },
]);
